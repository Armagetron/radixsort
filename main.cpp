#include <algorithm>
#include <chrono>
#include <vector>
#include <thread>
#include <iostream>
#include <string>
#include <sstream>
#include <tuple>
#include <gtest/gtest.h>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include <boost/range/algorithm.hpp>

#include "seq_msd_radix.h"
#include "paradis.h"

template<class T>
void print_vec(std::vector<T> &v) {
    std::stringstream ss;
    for (T i : v) {
        ss << i << " ";
    }
    ss << "\n";
    std::cout << ss.str();
}

TEST(histogram, level0_p1) {
    const auto BUCKET_SIZE = (1u << 8u);
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, 0, 1, 0, v.size());
    histogram(v, cnt2, 0, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level0_p2) {
    const auto BUCKET_SIZE = (1u << 8u);
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, 0, 2, 0, v.size());
    histogram(v, cnt2, 0, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level0_p8) {
    const auto BUCKET_SIZE = (1u << 8u);
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, 0, 8, 0, v.size());
    histogram(v, cnt2, 0, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level1_p1) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 1;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 1, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level1_p2) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 1;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 2, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level1_p8) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 1;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 8, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level2_p1) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 2;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 1, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level2_p2) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 2;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 2, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level2_p8) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 2;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 8, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level3_p1) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 3;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 1, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level3_p2) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 3;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 2, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(histogram, level3_p8) {
    const auto BUCKET_SIZE = (1u << 8u);
    const auto level = 3;
    std::vector<unsigned int> cnt(BUCKET_SIZE), cnt2(BUCKET_SIZE);
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    histogram(v, cnt, level, 8, 0, v.size());
    histogram(v, cnt2, level, 0, v.size());

    for(auto i = 0; i < BUCKET_SIZE; i++) {
        EXPECT_EQ(cnt[i], cnt2[i]);
    }
}

TEST(partitions, histogram) {
    std::vector<unsigned int> v(513);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    std::vector<unsigned long> starts, ends;

    PartitionForHistogram(0, v.size(), 8, starts, ends);
    EXPECT_EQ(true, true);
}

TEST(partitions, permuation) {
    const auto BUCKET_SIZE = (1u << 8u);
    std::vector<unsigned int> v(513);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    std::vector<unsigned long> starts, ends;
    std::vector<unsigned int> cnt(BUCKET_SIZE);

    PartitionForHistogram(0, v.size(), 8, starts, ends);
    histogram(v, cnt, 2, 8, 0, v.size());

    starts.clear();
    starts.resize(BUCKET_SIZE);
    ends.clear();
    ends.resize(BUCKET_SIZE);
    starts[0] = 0;
    ends[0] = cnt[0];
    for (unsigned int i = 1; i < BUCKET_SIZE; i++) {
        starts[i] = starts[i - 1] + cnt[i - 1];
        ends[i] = ends[i - 1] + cnt[i];
    }

    PartitionForPermutation(0, v.size(), 8, starts, ends, cnt);
    EXPECT_EQ(true, true);
}

TEST(partitions, repair) {
    const auto BUCKET_SIZE = (1u << 8u);
    std::vector<unsigned int> v(513);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    std::vector<unsigned long> starts, ends;
    std::vector<unsigned int> cnt(BUCKET_SIZE);

    PartitionForHistogram(0, v.size(), 8, starts, ends);
    histogram(v, cnt, 2, 8, 0, v.size());

    starts.clear();
    starts.resize(BUCKET_SIZE);
    ends.clear();
    ends.resize(BUCKET_SIZE);
    starts[0] = 0;
    ends[0] = cnt[0];
    for (unsigned int i = 1; i < BUCKET_SIZE; i++) {
        starts[i] = starts[i - 1] + cnt[i - 1];
        ends[i] = ends[i - 1] + cnt[i];
    }

    PartitionForRepair(0, v.size(), 8, starts, ends, cnt);
    EXPECT_EQ(true, true);
}

TEST(std_sort, std_sort) {
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    std::sort(v.begin(), v.end());
    EXPECT_EQ(std::is_sorted(v.begin(), v.end()), true);
}

TEST(seq_msd_radix, uniform) {
    std::vector<unsigned int> v(1000);
    std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
    boost::range::random_shuffle(v);

    msd_radix(v);
    EXPECT_EQ(std::is_sorted(v.begin(), v.end()), true);
}

TEST(paradis, ascending) {
    std::vector<unsigned int> v(10);
    std::generate(v.begin(), v.end(), [n = 253]() mutable { return n++; });
    boost::range::random_shuffle(v);
    auto length = v.size();

    print_vec(v);
    paradis(v);
    print_vec(v);
    EXPECT_EQ(length, v.size());
    EXPECT_EQ(std::is_sorted(v.begin(), v.end()), true);
}

GTEST_API_ int main(int argc, char *argv[]) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}