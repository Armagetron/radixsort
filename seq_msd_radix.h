#ifndef RADIXSORT_SEQ_MSD_RADIX_H
#define RADIXSORT_SEQ_MSD_RADIX_H

#include <vector>

void msd_radix(std::vector<unsigned int> &v);

#endif //RADIXSORT_SEQ_MSD_RADIX_H