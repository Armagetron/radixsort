#ifndef RADIXSORT_PARADIS_H
#define RADIXSORT_PARADIS_H

#include <vector>

void PartitionForHistogram(unsigned long start, unsigned long end, unsigned long p, std::vector<unsigned long> &starts, std::vector<unsigned long> &ends);
void PartitionForPermutation(unsigned long start, unsigned long end, unsigned long p,
                             std::vector<unsigned long> &starts, std::vector<unsigned long> &ends,
                             std::vector<unsigned int> &cnt);
void PartitionForRepair(unsigned long start, unsigned long end, unsigned long p,
                        std::vector<unsigned long> &starts, std::vector<unsigned long> &ends,
                        std::vector<unsigned int> &cnt);
void PartitionForRecursion();

void histogram(std::vector<unsigned int> &v, std::vector<unsigned int> &cnt, unsigned long l, unsigned long p,
                      unsigned long start, unsigned long end);

void histogram(std::vector<unsigned int> &v, std::vector<unsigned int> &cnt, unsigned long l, unsigned long start, unsigned long end);

void paradis(std::vector<unsigned int> &v);
void paradis(std::vector<unsigned int> &v, unsigned long p);

#endif //RADIXSORT_PARADIS_H
