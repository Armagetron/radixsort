#include <thread>
#include <iostream>
#include <sstream>
#include <boost/asio.hpp>
#include "paradis.h"
#include <queue>
#include <tuple>
#include <set>

enum {
    // radix bits per counting sort iteration
            RADIX = 8u,
    // bucket count per partition
            BUCKET_SIZE = (1u << RADIX) //B the range of b()
};

void PartitionForHistogram(unsigned long start, unsigned long end, unsigned long p, std::vector<unsigned long> &starts,
                           std::vector<unsigned long> &ends) {
    //Partitions over entire input and equally sized
    auto chunk = (end - start) / p;

    starts.clear();
    starts.resize(p);
    ends.clear();
    ends.resize(p);

    auto k = 0;
    for (auto i = start; i < start + chunk * p; i += chunk) {
        auto j = i + chunk;
        if (i == start + chunk * (p - 1) || j > end) {
            j = end;
        }
        starts[k] = i;
        ends[k++] = j;
    }

    //DEBUG OUTPUT ONLY
    /*for (unsigned long i = 0; i < p; i++) {
        std::cout << i << ": " << starts[i] << " : " << ends[i] << std::endl;
    }*/
}

void
PartitionForPermutation(unsigned long start, unsigned long end, unsigned long p, std::vector<unsigned long> &starts,
                        std::vector<unsigned long> &ends, std::vector<unsigned int> &cnt) {
    std::vector<std::vector<unsigned long>> ph(p);
    std::vector<std::vector<unsigned long>> pt(p);

    for (unsigned long i = 0; i < p; i++) {
        ph[i].resize(BUCKET_SIZE);
        pt[i].resize(BUCKET_SIZE);
    }

    for (unsigned int n = 0; n < BUCKET_SIZE; n++) {
        auto chunk = (ends[n] - starts[n]) / p;
        auto k = 0;
        for (auto i = starts[n]; i < starts[n] + chunk * p; i += chunk) {
            auto j = i + chunk;
            if (i == starts[n] + chunk * (p - 1) || j > ends[n]) {
                j = end;
            }
            ph[k][n] = i;
            pt[k++][n] = j;
        }
    }

    //DEBUG OUTPUT ONLY
    /*for (unsigned long i = 0; i < p; i++) {
        std::cout << "Processor " << i << ": " << std::endl;
        for (unsigned long k = 0; k < BUCKET_SIZE; k++) {
            std::cout << "\ti: " << k << " : " << ph[i][k] << " : " << pt[i][k] << std::endl;
        }
    }*/
}

void PartitionForRepair(unsigned long start, unsigned long end, unsigned long p,
                        std::vector<unsigned long> &starts, std::vector<unsigned long> &ends,
                        std::vector<unsigned int> &cnt) {
    std::priority_queue<std::tuple<unsigned long, unsigned long>, std::vector<std::tuple<unsigned long, unsigned long>>, std::greater<>> sizes;
    std::vector<std::set<unsigned long>> partition(p);
    for(unsigned long i = 0; i < p; i++) {
        sizes.emplace(std::tuple<unsigned long, unsigned long>(0, i));
    }

    for(unsigned long i= 0; i < BUCKET_SIZE; i++ ) {
        auto count = ends[i] - starts[i];
        if(count == 0) count = 1; //treat empty buckets with size one for better load balancing
        auto elem = sizes.top();
        sizes.pop();
        sizes.emplace(std::tuple<unsigned long, unsigned long>(std::get<0>(elem) + count, std::get<1>(elem)));
        partition[std::get<1>(elem)].emplace(i);
    }
    return;
}

void PartitionForRecursion() {

}

inline unsigned int radix(unsigned int value, unsigned long l) { //b function gibing bucket at level l
    return (value >> ((sizeof(unsigned int) - l - 1) * 8)) & (BUCKET_SIZE - 1u);
}

void histogram(std::vector<unsigned int> &v, std::vector<unsigned int> &cnt, unsigned long l, unsigned long p,
               unsigned long start, unsigned long end) {
    auto chunk = (end - start) / p;

    unsigned long starts[p];
    unsigned long ends[p];

    auto k = 0;
    for (auto i = start; i < start + chunk * p; i += chunk) {
        auto j = i + chunk;
        if (i == start + chunk * (p - 1) || j > end) {
            j = end;
        }
        starts[k] = i;
        ends[k++] = j;
    }

    std::vector<std::vector<unsigned int>> cnts(p);
    boost::asio::thread_pool pool(p);
    for (auto i = 0; i < p; i++) {
        cnts[i].resize(BUCKET_SIZE);
        boost::asio::post(pool, [=, &starts, &ends, &cnts]() {
            for (size_t j = starts[i]; j < ends[i]; j++) {
                cnts[i][radix(v[j], l)]++;
            }
        });
    }
    pool.join();

    for (unsigned long i = 0; i < p; i++) {
        for (unsigned long j = 0; j < BUCKET_SIZE; j++) {
            cnt[j] += cnts[i][j];
        }
    }
}

void histogram(std::vector<unsigned int> &v, std::vector<unsigned int> &cnt, unsigned long l, unsigned long start, unsigned long end) {
    for(auto n = start; n < end; n++) {
        cnt[radix(v[n], l)]++;
    }
}

inline unsigned long check_loop(const unsigned long *gh, const unsigned long *gt) {
    unsigned long sum = 0;
    for (unsigned long i = 0; i < BUCKET_SIZE; i++) {
        sum += gt[i] - gh[i];
    }
    return sum;
}

void paradis_recursion(std::vector<unsigned int> &v, unsigned long l, unsigned long p, unsigned long start,
                       unsigned long end) {
    std::vector<unsigned int> cnt(BUCKET_SIZE);
    histogram(v, cnt, l, p, start, end);

    unsigned long gh[BUCKET_SIZE];
    unsigned long gt[BUCKET_SIZE];

    unsigned long starts[BUCKET_SIZE];
    unsigned long ends[BUCKET_SIZE];

    starts[0] = gh[0] = start;
    ends[0] = gt[0] = start + cnt[0];
    for (unsigned int i = 1; i < BUCKET_SIZE; i++) {
        starts[i] = gh[i] = gh[i - 1] + cnt[i - 1];
        ends[i] = gt[i] = gt[i - 1] + cnt[i];
    }

    //PartitionForRepair
    std::vector<std::set<unsigned long>> partition(p);
    {
        std::priority_queue<std::tuple<unsigned long, unsigned long>, std::vector<std::tuple<unsigned long, unsigned long>>, std::greater<>> sizes;
        for (unsigned long i = 0; i < p; i++) {
            sizes.emplace(std::tuple<unsigned long, unsigned long>(0, i));
        }

        for (unsigned long i = 0; i < BUCKET_SIZE; i++) {
            auto count = gt[i] - gh[i];
            if (count == 0) count = 1; //treat empty buckets with size one for better load balancing
            auto elem = sizes.top();
            sizes.pop();
            sizes.emplace(std::tuple<unsigned long, unsigned long>(std::get<0>(elem) + count, std::get<1>(elem)));
            partition[std::get<1>(elem)].emplace(i);
        }
    }

    while (check_loop(gh, gt) > 0) {
        /*for(unsigned long i = 0; i < BUCKET_SIZE; i++) {
            std::cout << "i: " << i << " gh[i]: " << gh[i] << " gt[i]: " << gt[i] << std::endl;
        }*/
        //PartitionForPermutation
        std::vector<std::vector<unsigned long>> ph(p);
        std::vector<std::vector<unsigned long>> pt(p);

        for (unsigned long i = 0; i < p; i++) {
            ph[i].resize(BUCKET_SIZE);
            pt[i].resize(BUCKET_SIZE);
        }

        for (unsigned int n = 0; n < BUCKET_SIZE; n++) {
            auto chunk = (gt[n] - gh[n]) / p;

            if(chunk == 0) {
                if(gh[n] == gt[n]) {
                    for (unsigned long proc = 0; proc < p; proc++) {
                        ph[proc][n] = gt[n];
                        pt[proc][n] = gt[n];
                    }
                } else {
                    ph[0][n] = gh[n];
                    pt[0][n] = gt[n];
                    for (unsigned long proc = 1; proc < p; proc++) {
                        ph[proc][n] = gt[n];
                        pt[proc][n] = gt[n];
                    }
                }
            } else {
                auto k = 0;
                for (auto i = gh[n]; i < gh[n] + chunk * p; i += chunk) {
                    auto j = i + chunk;
                    if (i == gh[n] + chunk * (p - 1) || j > gt[n]) {
                        j = gt[n];
                    }
                    ph[k][n] = i;
                    pt[k++][n] = j;
                }
            }
        }
        /*for (unsigned long i = 0; i < p; i++) {
            std::cout << "Level " << l << " Processor " << i << ": " << std::endl;
            for (unsigned long k = 0; k < BUCKET_SIZE; k++) {
                if(pt[i][k] == 0) continue;
                std::cout << "\ti: " << k << " : " << ph[i][k] << " : " << pt[i][k] << std::endl;
            }
        }*/

        //PARADIS_Permute
        boost::asio::thread_pool pool(p);
        for (unsigned int n = 0; n < p; n++) {
            //boost::asio::post(pool, [=, &v, &ph, &pt, &cnt, &gh, &gt]() {
                for (unsigned long i = 0; i < BUCKET_SIZE; i++) {
                    auto head = ph[n][i];
                    while (head < pt[n][i]) {
                        auto value = v[head];
                        auto k = radix(value, l);
                        while (k != i && ph[n][k] < pt[n][k]) {
                            std::swap(value, v[ph[n][k]++]);
                            k = radix(value, l);
                        }
                        if (k == i) {
                            v[head++] = v[ph[n][i]];
                            v[ph[n][i]++] = value;
                        } else {
                            v[head++] = value;
                        }
                    }
                }
            //});
        }
        pool.join();

        //PARADIS_Repair
        boost::asio::thread_pool pool2(p);
        for (unsigned int n = 0; n < p; n++) {
            //boost::asio::post(pool2, [=, &v, &ph, &pt, &cnt, &gh, &gt, &partition]() {
                for(auto i : partition[n]) {
                    auto tail = gt[i];
                    for (unsigned long k = 0; k < p; k++) {
                        auto head = ph[k][i];
                        while (head < pt[k][i] && head < tail) {
                            auto value = v[head++];
                            if (radix(value, l) != i) {
                                while (head < tail) {
                                    auto w = v[--tail];
                                    if (radix(w, l) == i) {
                                        v[head - 1] = w;
                                        v[tail] = value;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    gh[i] = tail;
                }
            //});
        }
        pool2.join();
    }

    if ((l + 1) < sizeof(unsigned int)) {
        for (unsigned long i = 0; i < BUCKET_SIZE; i++) {
            if (ends[i] - starts[i] > 1) paradis_recursion(v, l + 1, p, starts[i], ends[i]);
        }
    }
}

void paradis(std::vector<unsigned int> &v) {
    paradis_recursion(v, 0, /*std::thread::hardware_concurrency()*/ 2, 0, v.size());
}

void paradis(std::vector<unsigned int> &v, unsigned long p) {
    paradis_recursion(v, 0, p, 0, v.size());
}