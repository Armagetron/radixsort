#include <cstring>
#include "seq_msd_radix.h"

enum {
    // radix bits per counting sort iteration
            RADIX = 8,
    // bucket count per partition
            BUCKET_SIZE = (1 << RADIX) //B the range of b()
};

inline unsigned int radix(unsigned int value, unsigned long l) { //b function gibing bucket at level l
    //convert level to shift
    //unsigned int shift = (sizeof(unsigned int) - l - 1) * 8;

    return (value >> ((sizeof(unsigned int) - l - 1) * 8)) & (BUCKET_SIZE - 1u);
}

void msd_radix_recursion(std::vector<unsigned int> &v, unsigned long l, unsigned long start, unsigned long end) {
    unsigned int cnt[BUCKET_SIZE];
    std::memset(cnt, 0, sizeof(unsigned int) * BUCKET_SIZE);

    for(auto n = start; n < end; n++) {
        cnt[radix(v[n], l)]++;
    }

    unsigned long gh[BUCKET_SIZE];
    unsigned long gt[BUCKET_SIZE];

    unsigned long starts[BUCKET_SIZE];
    unsigned long ends[BUCKET_SIZE];

    starts[0] = gh[0] = start;
    ends[0]   = gt[0] = start + cnt[0];
    for(unsigned int i = 1; i < BUCKET_SIZE; i++) {
        starts[i] = gh[i] = gh[i - 1] + cnt[i - 1];
        ends[i]   = gt[i] = gt[i - 1] + cnt[i];
    }

    for(unsigned long i = 0; i < BUCKET_SIZE; i++) {
        while(gh[i] < gt[i]) {
            auto value = v[gh[i]];
            while(radix(value, l) != i) {
                std::swap(value, v[gh[radix(value, l)]++]);
            }
            v[gh[i]++] = value;
        }
    }

    if(l < sizeof(unsigned int)) {
        for(unsigned long i = 0; i < BUCKET_SIZE; i++) {
            if (ends[i] - starts[i] > 1) msd_radix_recursion(v, l + 1, starts[i], ends[i]);
        }
    }
}

void msd_radix(std::vector<unsigned int> &v) {
    msd_radix_recursion(v, 0, 0, v.size());
}